const express = require('express'),
    fs = require('fs'),
    path = require('path'),
    Note = require('./Note');

const app = express();

const hostname = '127.0.0.1',
    port = 8000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.resolve(__dirname + '/')));

app.get('/', function (req, res) {
    res.send(readFile());
});

app.post('/', function (req, res) {
    (new Note()).add(req.body);
    res.redirect('/');
});

app.get('/list', function (req, res) {
    res.json({ list: (new Note()).getList() });
});
app.get('/show', function (req, res) {
    res.send(readFile(((new Note()).getDetail(req.query.id))));
});
app.get('/delete', function (req, res) {
    (new Note()).delete(req.query.id);
    res.redirect('/');
});


app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});

function readFile(insert = false) {
    try {
        const data = fs.readFileSync('index.html', 'utf-8');
        if (data && insert)
            return data.replace('TEXT', insert)
        else {
            return data.replace('TEXT', '');
        }
    } catch (err) {
        console.error(err);
    }
    return false;
}
