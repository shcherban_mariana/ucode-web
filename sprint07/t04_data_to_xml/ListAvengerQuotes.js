const fs = require('fs'),
    xml2js = require('xml2js');

module.exports.ListAvengerQuotes = class {
    dataXML = '';
    toXML(file) {
        let builder = new xml2js.Builder();
        this.dataXML = builder.buildObject(this.data);

        try {
            fs.writeFile(file, this.dataXML, (err) => {
                if (err) console.log(err);
            });
        } catch (err) {
            console.error(err)
        }
    }

    fromXML(file) {
        this.dataXML = fs.readFileSync(file, 'utf-8');
        return this.dataXML;
    }
}
