const Model = require('../model');

module.exports = class Hero extends Model {
    constructor(id, name, description, class_role, race_id) {
        super(id, name, description, class_role, race_id)
    }
    find(id) {
        super.find(id)
    }
    delete() {
        super.delete()
    }
    save() {
        super.save()
    }
}
