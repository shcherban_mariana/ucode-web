const fs = require('fs');

module.exports = class {
    dir = 'tmp';
    list = [];

    constructor() {
        this.getList();
    }

    getList() {
        return fs.readdirSync(this.dir)
    }

    hasFiles() {
        return this.list
            ? true
            : false;
    }

    getHTMLList() {
        let render = '<ul>';
        this.list.map(item => {
            render += '<li data-file="' + item + '" class="btn-file">' + item + '</li>';
        });
        render += '</ul>';
        return render;
    }
}
