const express = require('express'),
    cookieSession = require('cookie-session');

const app = express();

const hostname = '127.0.0.1',
 port = 8000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.set('trust proxy', 1);

app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2'],
    maxAge: 24 * 60 * 60 * 1000
}));

app.get('/', (req, res, next) => {
    req.session.views = (req.session.views || 0) + 1;
    res.end(`
    <h1>Cookie counter</h1>
    <p>This page was loaded ${req.session.views} time(s) in last minute`);
});

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
