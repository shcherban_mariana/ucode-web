USE ucode_web;
INSERT INTO powers (hero_id, name, type, points)
VALUES
('1', 'bloody fist', 'attack', '110'),
('2', 'iron shield', 'defense', '200'),
('3', 'iron shield', 'defense', '253'),
('4', 'bloody fist', 'attack', '346');

INSERT INTO races (hero_id, name)
VALUES
('1', 'Human'),
('2', 'Kree');

INSERT INTO teams (hero_id, name)
VALUES
('1', 'Hydra'), 
('2', 'Avengers'),
('3', 'Hydra');
