const express = require('express'),
Iconv = require('iconv').Iconv,
 app = express();

const hostname = 'localhost',
 port = 8000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let memory = '';

app.get('/', (req, res) => {
    res.sendfile('index.html');
});

app.post('/', (req, res) => {
    memory = req.body;
    if (memory.inputText && memory.charset) {
        res.send(render(getCharset(memory)));
    }
    else {
        res.sendfile('index.html');
    }
});

function getCharset(param) {
    let result = `<p>Input string:<textarea type="currnet_charset" cols="20" rows="3" placeholder="${param.inputText}"></textarea><p>`;
    if (typeof param.charset === 'string') {
        return result + getString(param.charset, param.inputText);
    }
    else {
        param.charset.forEach((item) => {
            result += getString(item, param.inputText);
        });
        return result;
    }
}

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
