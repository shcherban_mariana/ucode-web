USE ucode_web;
DROP TABLE IF EXISTS heroes;
CREATE TABLE heroes(
    id INT AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL UNIQUE,
    description TEXT NOT NULL,
    class_role ENUM('tankman', 'healer', 'dps') NOT NULL,
    PRIMARY KEY(id)
);
