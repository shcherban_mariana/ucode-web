const fs = require('fs');

module.exports = class {
    list = [];
    constructor() {

        const data = fs.readFileSync('NotePad.js', 'utf-8');
        this.note = JSON.parse(data 
            ? data 
            : '{}');
        for(let key in this.note) {
            this.list.push([key, this.note[key]]);
        }
    }

    getList() {
        return this.list;
    }

    get(id) {
        return (new Map(this.list)).get(id);
    }

    delete(id) {
        delete this.note[id];
        try {
            fs.writeFile('NotePad.js', JSON.stringify(this.note), (err) => {
                if (err) console.log(err);
            })
        } catch (err) {
            console.error(err)
        }
    }
}

