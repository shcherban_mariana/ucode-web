function getHtml(data){
   let result = '';
   for(let key in data) {
       if(typeof data[key]  === "object") {
           result += `<div class="box"><b class="key">${key}</b>: ${getHtml(data[key])}</div>`;
       } else {
           result += `<div class="box"><b>${key}</b>: ${data[key]}</div>`;
       }
   }
   return result;
}
