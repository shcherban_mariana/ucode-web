const express = require('express'),
    fs = require('fs'),
    request = require('request');

const app = express();

const hostname = '127.0.0.1',
    port = 8000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    console.log(req.query);
    console.log(req.query.url);

    if (req.query.url) {
        request(req.query.url.includes('http')
            ? req.query.url
            : 'http://' + req.query.url, function (res, body) {
                res.send(readFile('<hr>url: ' +
                    req.query.url + '<br><hr>' +
                    body.substring(body.indexOf('<body'),
                        body.indexOf('</body') + 7).replaceAll('<', '&lt;').replaceAll('>', '&gt')));
            });
    } else {
        res.send(readFile('<div id="typeurl">Type an URL...</div>'));
    }
});

function readFile(insert = false) {
    try {
        const data = fs.readFileSync('index.html', 'utf-8');
        return (data && insert) ? data.replace('#TEXT#', insert) : data.replace('#TEXT#', '');
    } catch (err) {
        console.log(err);
    }
    return false;
}

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
