const mysql = require('mysql');
const config = require('./config.json');

class Model {
    constructor(id, name, description, class_role, race_id) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.class_role = class_role;
        this.race_id = race_id;
    }

    find(id) {
        mysql.connect();

        let query = `SELECT * FROM heroes WHERE id = ${id}`;

        mysql.query(query, id, function (err, rows) {
            if (err) {
                console.log('Hero wasn`t found:(');
                throw err;
            }
            this.id = rows[0].id;
            this.name = rows[0].name;
            this.description = rows[0].description;
            this.class_role = rows[0].class_role;
            this.race_id = rows[0].race_id;

            console.log(`Here is: ${this.name}`);
        })
    }

    delete() {
        mysql.connect();

        if (this.id === null)
            throw new Error('ID is not exists');

        let query = `DELETE FROM heroes WHERE id = ${this.id}`;

        mysql.query(query, function (err, result) {
            if (err) throw err;
            console.log('Hero was deleted: ' + result);
        })
    }

    save() {
        mysql.connect();

        if (this.id === null)
            throw new Error('ID is not exists');

        let query = `INSERT INTO heroes WHERE id = ${this.id}`;

        mysql.query(query, function (err, rows) {
            if (err) throw err
        })
    }
}

module.exports = Model;
