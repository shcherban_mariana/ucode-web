const express = require('express'),
    fs = require('fs');

const app = express();

const hostname = '127.0.0.1',
    port = 8000;

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
