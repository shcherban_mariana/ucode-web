const express = require('express'),
    session = require('express-session');

const app = express();
const router = express.Router();

const hostname = '127.0.0.1',
    port = 8000;

//session middleware
app.use(session({
    secret: "thisismysecretkey",
    saveUninitialized: true,
    resave: false
}));

// parsing the incoming data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let sess;

router.get('/', function (req, res) {
    sess = req.session;
    if (sess.name)
        return res.redirect('admin');
    res.sendFile(__dirname + '/index.html')
});


router.post('/login', (req, res) => {
    let res1 = 1;
    let res2 = 1;
    sess = req.session;
    console.log(req.body)
    req.body.forEach(element => {
        if (element.includes('powers_'))
            sess.exp = res1++
        else {
            sess.exp = res1 - 1
        }
        if (element.includes('pub_'))
            sess.purpose = res2++
        else {
            sess.purpose = res2 - 1
        }
    })
    res.redirect("/admin")
});

router.get('/admin', (req, res) => {
    sess = req.session;
    if (sess.name && sess.alias && sess.age && sess.text && sess._file) {
        res.write(`<h1>Session for new</h1>
        <pre>
        name:${sess.name}
        alias:${sess.alias}
        age:${sess.age}
        description:${sess.text}
        photo:${sess._file}
        expirience:${sess.exp}
        level:${sess.level_control}
        purpose:${sess.purpose}</pre>`);
        res.end('<button><a href=' + '/logout' + '>Forget</a></button>');
    } else {
        res.write('<h1>Login, please</h1>');
        res.end('<a href=' + '/' + '>Login</a>');
    }
})

router.get('/logout', (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return console.log(err);
        }
        res.redirect('/');
    });

});

app.use('/', router);

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});

