USE ucode_web;
INSERT INTO heroes (name, description, class_role)
VALUES
  (
    'Iron Man',
    'Iron Man was a technical leader in The Avengers, considering he was the smartest and was the first-billed in the cast of The Avengers, while Captain America was the the co leader of The Avengers due to his miltary experience, good strategies, level-headedness and good leadership qualities.',
    'dps'
),
  (
    'Captain America',
    'Captain America is traditionally seen as one of the greatest heroes of the Marvel Universe. He is a righteous and brave man who always wants to see the good in people.',
    'dsp'
),
  (
    'Spider-Man',
    'Spider-Man is a superhero appearing in American comic books published by Marvel Comics.',
    'tankman'
),
  (
    'Rocket',
    'Rocket Raccoon insisted that he`s the Guardians of the Galaxy`s captain, and yet he was suddenly okay letting Star-Lord be the team`s leader in Avengers:',
    'tankman'
),
  (
    'Black Panther',
    'Superhuman strength, speed, stamina, reflexes, endurance, and durability. He also has enhanced healing capabilities and superhuman senses, similar to a wildlife panther.',
    'tankman'
),
  (
    'Ant-Man',
    'He was a member of the Avengers, the Fantastic Four and the Guardians of the Galaxy, the main character in the comic-book series FF and, in 2015, he became the title character in the series Ant-Man.',
    'healer'
),
  (
    'Hulk',
    'The Hulk is a superhero/antihero in the Marvel Universe, known for both his super strength and his seemingly unstoppable rages.',
    'dps'
),
  (
    'Winter Soldier',
    'The Falcon and The Winter Soldier confirms the world views Bucky Barnes as an Avenger, and it only took the MCU 10 years to make him a hero.',
    'healer'
),
  (
    'Groot',
    'Groot is a fictional superhero appearing in American comic books published by Marvel Comics.',
    'healer'
),
  (
    'Thor',
    'Thor Odinson, or simply Thor, is a fictional character appearing in American comic books published by Marvel Comics.',
    'tankman'
);
