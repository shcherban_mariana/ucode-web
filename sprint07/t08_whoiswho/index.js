const { readFile, renderTable } = require('./parserCSV')

const express = require('express'),
    session = require('express-session'),
    fs = require('fs'),
    csv = require('csv-parser'),
    multer = require('multer'),
    path = require('path');

const app = express();

const hostname = '127.0.0.1',
    port = 8000;

let csvArr = [],
    sess;

app.use(session({ secret: 'thisismesecretkey', saveUninitialized: true, resave: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/uploads', express.static(__dirname + '/'));
app.use(multer({ dest: 'uploads' }).single('file'));
app.use(express.static(path.resolve(__dirname + '/')));

app.get('/', (req, res) => {
    res.send(readFile());
});

app.post('/', (req, res, next) => {
    sess = req.session;
    if (!req.file) {
        res.redirect('/');
    } else {
        sess.file = req.file.path;

        let results = '';
        fs.createReadStream(sess.file)
            .pipe(csv())
            .on('data', (data) => csvArr.push(data))
            .on('end', () => {
                results = renderTable(csvArr);
                res.send(readFile(results));
            });
    }
});

app.get('/filter', (req, res) => {
    let results = '';
    fs.createReadStream(sess.file)
        .pipe(csv())
        .on('data', (data) => csvArr.push(data))
        .on('end', () => {
            results = renderTable(csvArr, req.query);
            res.send(readFile(results));
        });
});

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});

