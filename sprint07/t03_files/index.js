const express = require('express'),
    path = require('path'),
    fs = require('fs');

const File = require('./File.js'),
    FileList = require('./FileList.js');

const hostname = '127.0.0.1',
    port = 8000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.resolve(__dirname + '/')));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.post('/', (req, res) => {
    let file = new File(req.body.filename);
    file.write(req.body.content);
    res.redirect('/');
});

app.get('/list', (req, res) => {
    let fileList = new FileList();
    res.json({ html: fileList.getHTMLList() });

    console.log(fileList.getList());
    console.log(fileList.hasFiles());
    console.log(fileList.getHTMLList())
});

app.get('/show', (req, res) => {
    console.log(req.query.file);
    let file = new File(req.query.file);
    res.json({ content: file.read() });
});

app.get('/delete', (req, res) => {
    let fileList = new FileList();
    let file = new File(req.query.file);

    file.delete();

    console.log(fileList.getList());

    res.redirect('/');
});

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
