const express = require("express");
const session = require("express-session");
const bcrypt = require("bcrypt");
const app = express();

const hostname = '127.0.0.1',
    port = 8000;

//session middleware
app.use(session({
    secret: "thisismysecretkey",
    saveUninitialized: true,
    resave: false
}));

// parsing the incoming data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let sess;
app.get('/', function (req, res) {
    sess = req.session;
    if (sess.hash && sess.hash !== '') {
        return res.render('hacked', {
            hash: sess.hash
        });
    }
    return res.render('index');
});

app.post('/', (request, response) => {
    sess = request.session;
    console.log(sess);
    sess.password = request.body.password;
    sess.rounds = request.body.rounds;
    console.log(sess.rounds);
    sess.hash = bcrypt.hashSync(sess.password, sess.rounds);
    response.redirect('/admin');
});

app.get('/logout', (request, response) => {
    request.session.destroy(err => {
        if (err) {
            return console.log(err)
        }
    })
    response.redirect('/')
})

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
