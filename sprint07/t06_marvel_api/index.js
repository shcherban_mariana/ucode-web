const express = require('express'),
    fetch = require('node-fetch'),
    crypto = require('crypto');

const app = express();

const hostname = '127.0.0.1',
    port = 8000;

const pubKey = '';
const privKey = 'AIzaSyBGnhy8yfj7ZR11_Y3D9ipX_GTrqKWTwr4';


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', async function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
