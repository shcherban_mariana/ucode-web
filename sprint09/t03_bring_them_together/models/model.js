const connecting = require("./db");
const mysql = require('mysql2');

class Model {
    constructor(table) {
        this.table = table
    }

    find(id) {
        mysql.connect();

        let query = `SELECT * FROM ${this.table} WHERE id = ${id}`;

        mysql.query(query, id, function (err, rows) {
            if (err) {
                console.log('There is no user');
                throw err;
            }
            if (rows[0]) {
                for (let key in rows[0]) {
                    this[key] = rows[0][key];
                }
                return 0;
            }
        })
    }

    async save() {
        let keys = [];
        let update = [];
        let values = [];
        
        for (let key in this) {
            if (key !== "id") {
                keys.push(key);
                values.push(`'${this[key]}'`);
                update.push(`${key}='${this[key]}'`);
            }
        }

        let query;

        if (this.id) {
            query = `UPDATE ${this.table} SET ${update} WHERE id=${this.id};`;
        } else {
            query = `INSERT ${this.table} (${keys}) VALUES (${values});`;
        }
        await connecting(query);
        query = `SELECT * FROM ${this.table} WHERE name = '${this.name}';`;
    }
}

module.exports = Model
