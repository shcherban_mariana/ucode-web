const express = require('express');
const path = require('path');
const mysql = require('mysql2');
const session = require('express-session');
const config = require('./config.json');

let app = express();

const hostname = '127.0.0.1',
	port = 8000;

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'static')));

app.get('/', function (request, response) {
	response.sendFile(path.join(__dirname + '/public/index.html'));
});

app.get('/styles.css', function (request, response) {
	response.sendFile(path.join(__dirname + '/public/styles.css'));
});

app.post('/auth', function (request, response) {
	let email = request.body.email;
	let login = request.body.login;
	let fullname = request.body.fullname;
	let password = request.body.password;

	if (!request.body)
		return response.sendStatus(400);

	if (email && login && fullname && password) {
		config.query('SELECT * FROM users WHERE email = ? AND login = ? AND fullname = ? AND password = ?', [email, login, fullname, password], function (error, results, fields) {
			if (error) throw error;
		});
	} else {
		let res = `
		<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
		</head>
		<style> 
		.success-data {
			display: flex;
			flex-direction: column;
			color: #eee;
		}
		
		.bxs-badge-check {
			font-size: 90px;
		}	
		</style>
		<body>
		<div class="success-data">
			<div class="text-center d-flex flex-column"> <i class='bx bxs-badge-check'></i> <span
				class="text-center fs-3">Your has been created <br> Successfully</span> </div>
		</div>
		</body>
		`;
		response.send(res);
		response.end();
	}
});

app.listen(port, hostname, () => {
	console.log(`Server is running at http://${hostname}:${port}/`);
});
