const Model = require('../model');

module.exports = class User extends Model {
    constructor() {  
        super('users');
    }
}
