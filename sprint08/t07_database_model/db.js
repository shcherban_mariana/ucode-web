const mysql = require('mysql2');
const config = require('./config.json');

const dbConnection = mysql.createConnection(config);

module.exports = dbConnection;
