const fs = require('fs');

function renderTable(arr, filter = false) {
    let map = getFilters(arr);
    let result = '<form action="/filter" id="filters"><table border="1px;"><tr>';
    for (let key in arr[0]) {
        result += `<th>${getFilterHtml(key, map, filter 
            ? filter[key] 
            : false)}</th>`;
    }
    result += '</tr>';
    if (filter && Object.keys(filter).length !== 0) {
        arr = arr.filter(item => {
            let flag = true;
            for (let key in item) {
                if (!(filter[key] === item[key] || filter[key] === 'all-items')) {
                    flag = false;
                }
            }
            return flag;
        });
    }
}

function getFilters(arr) {
    let map = new Map();
    for (let key in arr[0]) {
        map.set(key, [...new Set(arr.map(item => { return item[key] }))].sort());
    }
    return map;
}

function readFile(insert = false) {
    try {
        const data = fs.readFileSync('index.html', 'utf-8');
        if (data && insert)
            return data.replace('TEXT', insert)
        else {
            return data.replace('TEXT', '');
        }
    } catch (err) {
        console.error(err);
    }
    return false;
}

module.exports = { readFile, renderTable }

