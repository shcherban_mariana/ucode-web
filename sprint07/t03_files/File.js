const fs = require('fs');

module.exports = class {
    dir = 'tmp';

    constructor(name) {
        if (name.includes('')) {
            this.name = name;
        } else {
            this.name = name + '.txt';
        }
        this.filePath = this.dir + '/' + this.name;
    }

    create() {
        fs.writeFile(this.filePath, '', function (err) {
            if (err) throw err;
        });
    }

    write(content) {
        fs.writeFile(this.filePath, content, err => {
            if (err) {
                console.error(err)
                return
            }
        })
    }

    read() {
        fs.readFile(this.filePath, (err, data) => {
            if (err) {
              console.error(err)
              return
            }
            console.log(data)
          })
    }

    delete() {
        fs.unlink(this.filePath, function (err) {
            if (err) throw err;
            console.log('File deleted!');
        });
    }
}
