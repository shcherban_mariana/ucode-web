USE ucode_web;
SELECT heroes.name FROM heroes LEFT JOIN teams ON heroes.id = teams.hero_id
WHERE NOT heroes.race = 'human' AND heroes.name LIKE '%a%' AND heroes.class_role IN ('tankman', 'healer' )
GROUP BY heroes.id
ORDER BY heroes.id DESC;
