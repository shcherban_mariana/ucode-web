const express = require('express'),
    xml2js = require('xml2js'),
    { ListAvengerQuotes } = require('./ListAvengerQuotes');

const app = express();

const hostname = '127.0.0.1',
    port = 8000;

let list;

app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(express.static(__dirname + '/'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
app.get('/toXML', function (req, res) {
    list = new ListAvengerQuotes(data.data);
    list.toXML('./AvengerQuote.xml');
    res.json({ to: JSON.stringify(list.data), from: list.fromXML('AvengerQuote.xml') });
});

app.listen(port, hostname, () => {
    console.log(`App is running at http://${hostname}:${port}/`);
});
