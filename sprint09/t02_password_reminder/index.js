const express = require('express');
const nodemailer = require('nodemailer');
const path = require('path');
const mysql = require('mysql2');
const session = require('express-session');
const config = require('./config.json');
const User = require('./models/user');

let app = express();

const hostname = '127.0.0.1',
	port = 8000;

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'static')));

app.get('/', function (request, response) {
	response.sendFile(path.join(__dirname + '/views/index.html'));
});

app.get('/public/styles.css', function (request, response) {
	response.sendFile(path.join(__dirname + '/public/styles.css'));
});

app.get('/email_send', function (request, response) {
	response.sendFile(path.join(__dirname + '/views/pass.html'));
});

app.post('/reminder', function (request, response) {
	response.redirect('/email_send');
});

app.post('/email_send', function (request, response) {
	let user = new User(request.body);

	let mail = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: '',
			pass: ''
		}
	});

	let mailOptions = {
		from: 'userdev@gmail.com',
		to: user.email,
		subject: 'Check the password',
		html: `<p>Hi! This is your password: ${user.password}</p>`
	};

	mail.sendMail(mailOptions, function (error, info) {
		if (error) {
			response.send('Some problem:(');
		} else {
			let res = `
		<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../public/styles.css">
		</head>
		<style> 
		.success-data {
			display: flex;
			flex-direction: column;
			color: #eee;
		}
		
		.bxs-badge-check {
			font-size: 90px;
		}	
		</style>
		<body>
		<div class="success-data">
			<div class="text-center d-flex flex-column"> <i class='bx bxs-badge-check'></i> <span
				class="text-center fs-3">Email has been sent successfully</span> </div>
		</div>
		</body>
		`;
			response.send(res);
		}
	});


});

app.listen(port, hostname, () => {
	console.log(`Server is running at http://${hostname}:${port}/`);
});
